import os
from .get_sensor_vals import get_accel_gyro
from multiprocessing import Pool
from .MPU6050 import MPU6050
from os.path import isfile, join
import io
import socket
import datetime
from .NCS_MobileSSD import image_classify
from .raspi_bluetooth import getTargetBluetoothDevice, sendMessageTo, receiveMessages
from .camera_operations import record_and_capture
from .streamer import MyOutput
from mvnc import mvncapi as mvnc
from queue import Queue
from threading import Thread, Event, Lock
import cv2
import sys
import time
import config
import random


def write_now():
    # Circular streaming:
    #https://picamera.readthedocs.io/en/release-1.10/recipes1.html  공식 파이 카메라 라이브러리 doc
    # 마지막 random.randint() 대신에 CV 판독 TRUE 조건을 여기서 구현해야한다.
    # Randomly return True (like a fake motion detection routine)

    #https: // picamera.readthedocs.io / en / release - 1.10 / recipes2.html?highlight = splitter
    # 위의 링크 : The camera is capable of recording multiple streams at different resolutions simultaneously by use of the video splitter

    return random.randint(0, 10) == 0

def write_video(stream, current_time):
    print('Writing video!')
    with stream.lock:
        # Find the first header frame in the video
        for frame in stream.frames:
            if frame.frame_type == picamera.PiVideoFrameType.sps_header:
                stream.seek(frame.position)
                break
        # Write the rest of the stream to disk
        with io.open(current_time+'.h264', 'wb') as output:
            output.write(stream.read())

def run_main():

    isPoweredOn = True
    while isPoweredOn:
        #Spawn a thread that does the following, only stopping when len(bluetooth_detected > 0: 
        tid_bluetooth = Thread(target=getTargetBluetoothDevice) # 이 부분에서 블루투스 감지 --> 연결까지 해야된다.
        tid_bluetooth.start()

        tid_record_and_capture = Thread(target=record_and_capture)

        #bluetooth_detected= lookUpNearbyBluetoothDevices()
        #if len(bluetooth_detected) > 0: #
        if config.bluetooth_target_address is not None: #운전자의 안드로이드 블루투스 주소를 찾았다면~
            #connect to the Android
            #calibrate current time with Android
            #Split video outputstream to OuputStream1 and OutputStream2
            
        
        #Spawn a thread that does the following:
        accel, gyro = get_accel_gyro()
        


        #raspi_output = MyOutput()
        #stream = io.BytesIO()

        #Video Streaming
        #비디오 저장 (+ in-memory Streaming)

        #Time Stamp 관리




lastresults = None
frameBuffer = []
fps = ""





def set_VPU_and_Graph():
    # Movidius에서 예측시 표시할 로그 정보를 정한다  1: DEBUG, 2: INFO, 3: 경고 4: 에러, 5: 치명적인 오류
    mvnc.global_set_option(mvnc.GlobalOption.RW_LOG_LEVEL, 2)
    device = get_mvnc_devices()[0] # Movidius Neural Compute 기기 정보를 가져온다.
    devHandle = []
    graph = []
    graphHandle = []

    with open(join(get_graph_folder(), "super_crack_graph"), mode="rb") as f:
        graph_buffer = f.read()

    graph.append(mvnc.Graph('MobileNet-SSD' + str(0)))
    devHandle.append(mvnc.Device(device))
    devHandle[0].open()
    graphHandle.append(graph[0].allocate_with_fifos(devHandle[0], graph_buffer))

    print("")
    print("그래프 로딩 완료.")

    cam = cv2.VideoCapture(0)

    if cam.isOpened() != True:
        print("카메라 에러")
        quit()

    lock = Lock()
    results = Queue()
    # V: Vehicles (챠량), L: Lane (차선) , M: Manhole(맨홀), D: Drain(하수구)
    # P: Pothole (노면파임, 포트홀) , C : Crack (노면 균열), S : Sign (신호 표지판)
    LABELS = ('background',
              'V', 'L', 'M', 'D',
              'P', 'C', 'S')
    return graph, graph_buffer, graphHandle, lock, results, LABELS, lastresults

def get_graph_folder():
    graph_folder = "./"

    if len(sys.argv) > 1:
        graph_folder = sys.argv[1]
    return graph_folder

def get_mvnc_devices():

    devices = mvnc.enumerate_devices()
    if len(devices) == 0:
        print("NCS / VPU 인식 실패")
        quit()
    print(len(devices))
    return devices


if __name__ == '__main__':
    run_main()