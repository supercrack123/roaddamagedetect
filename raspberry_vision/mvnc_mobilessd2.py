from mvnc import mvncapi as mvnc
import numpy as np
import cv2
import time
import matplotlib.pyplot as plt

# 코드 참고 : https://www.youtube.com/watch?v=6ZKIjtK6-ug
#https://www.youtube.com/watch?v=P4eqeB1OZf4


def preprocess(src): # src == 인풋 이미지
    img = cv2.resize(src, input_size)
    img = img - 127.5  # 300 x 300 규격에 맞게 전처리
    img = img / 127.5
    return img.astype(np.float16) #


def predict(image, graph):
    # 전처리를 우선 적용하고
    image_pro = preprocess(image)
    label = None
    # 이미지 정보를 NCS 에 전송한 후 , FORWARD PASS 를 한번 이행 ===> 결과물 : 인공신경망 예측
    graph.LoadTensor(image_pro, None) # None 을 안넣어주면 에러가 발생한다.
    output, _ = graph.GetResult() # output은 사물클래스 예측결과 리스트

    #print(output)  ===> array([ , , , , , ] 여기서 output[0] 에 해당하는 것은 총 예측된 후보군 클래스 갯수이다.

    valid_boxes = int(output[0]) # 몇개의 박스로 예측했는지 . 인트로 변환해줘야 에러가 안난다.

    # 사물 클래스 prediction들 중 유효한것 ( 확신도)들을 가져와서
    # 예측 결과 리스트를 구성한다.

    # 각 박스별로 7개의  인덱스 자리를 차지하는 것으로 보이기 때문에, 이후 클래스 갯수 * 7 만큼을 한번의 루프마다 돌아야한다.
    # 각 클래스에서 베이스라인 인텍스가 base_index 일떄,

    # base_index +1 : 예상 사물 클래스
    # base_index +2 : 예상 확신도
    # 그 클래스의 예측된 바운딩박스의 x1 y1, x2, y2 에 해당하는 값들은
    # base_index + 3, +4, +5, +6 에 위치한다.

    # 물론 , 이 각각의 x1,y1 ,x2 , y2 값에  각각 원래 이미지의 가로 혹은 세로 길이인
    #  * w 혹은 * h를 곱해줘야한다.
    for i in range(7, 7*(1 + valid_boxes), 7): # output의 첫 7개 정보는 크게 필요없는  값들이어서
    #for looop의 세번쨰인자 : 7개 인데스씩 을 한 step 으로 지정.
        if not np.isfinite(sum(output[i+1 : i+7])): # output 부분에 NaN이라는 값이 나오는데, 그런 경우 valid한 판독 결과가 아니다. NaN 가 섞여있다면 해당 열의 합은  finite가 아니므로
            continue
        clss = CLASSES[int(output[i+1])] # 사물 클래스 정보를 가져온다.
        conf = output[i+2] #

        x1 = max(0, int(output[i + 3] * width)) # max 를 취하는 이유는 신경망 판독 결과로 바운더리 박스가 이미지 바운더리를 넘어가는 현상에 대처하기 위해 (x1 이 왼쪽 모서리이므로 max)
        y1 = max(0, int(output[i + 4] * height)) # width 혹은 height를 곱해주는 이유는 output 행렬이 float고, 바운더리 박스 좌표가 각 축의 비율로 표시되기 때문
        x2 = min(width, int(output[i+5] * width))
        y2 = min(height, int(output[i+6] * height))

        # 나중에 오버레이에서 출력할 스트링, {클래스} {확신도}
        label = '{} {}'.format(clss, conf * 100)
        """
        #오버레이 예시 
        image = cv2.rectangle(image, (x1, y1), (x2, y2), color,3) # 직사각형을 좌하단 , 우상단 꼭짓점으로 정의
        image = cv2.putText(image, label, (x1-5, y1-5), cv2.FONT_HERSHEY_SIMPLEX, 1, color,5) # color : 바운더리 박스 컬러 (랜덤함수)
        """
    return label


# MAIN #######


GRAPH = 'super_crack_graph'
#IMAGE = 'images/G0029965.JPG'  # positive data from M.J. Booysen Pothole dataset
CLASSES = ('background',
              'V', 'L', 'M',
              'P', 'C', 'S')

input_size = (300, 300) # 300 x 300 으로 인풋을 지정.
np.random.seed(3)
colors = 255 * np.random.rand(len(CLASSES), 3)

# NCS 장치 인식
devices = mvnc.EnumerateDevices()
device = mvnc.Device(devices[0])

device.OpenDevice()

# 훈련된 인공 신경망을 NCS 장치에 올린다.
with open(GRAPH, 'rb') as f:
    graph_file = f.read() # 컴파일된 그래프를 읽어들인다. ( NCS에 맞게 컴파일된 그래프)
graph = device.AllocateGraph(graph_file) # graph 는 여기서 MVNC graph 객체를 의미.

#print(graph)   # 그래프가 잘 로드 됐는지 확인

#image = cv2.imread(IMAGE)
capture = cv2.VideoCapture(0)
_, image = capture.read()# 한프레임을 먼저 읽음으로써 다음 줄에서 height, width 에 대한 정보를 가져올 수 있도록
height, width = image.shape[:2] # 첫 두 요소를 취한다. (세로, 가로 길이)
# 튜토리얼에서는 720 x 720 이었으나 우리의 경우 3760 x 2???

while True:
    stime = time.time() # 매 루프의 시작에서 현재시각을 측정
    _, image = capture.read()
    predict(image, graph)
    cv2.imshow('frame', image)
    print('FPS = {:.1f}'.format(1/(time.time() - stime))) # FPS 를 계산하는 부분
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

capture.release()
cv2.destroyAllwindows()
device.CloseDevice()   # 프로그램 종료시 NCS 디바이스 닫기






