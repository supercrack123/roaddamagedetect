from mpu6050 import mpu6050


def get_accel_gyro():

    sensor = mpu6050(0x68)
    accelerometer_data = sensor.get_accel_data()
    gyro_data = sensor.get_gyro_data()

    return accelerometer_data, gyro_data




