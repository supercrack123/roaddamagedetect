
#출처: https://gist.github.com/keithweaver/3d5dbf38074cee4250c7d9807510c7c3
#SuperCrack 프젝트에 알맞게 변경.
# Uses Bluez for Linux
#
# sudo apt-get install bluez python-bluez
#
# Taken from: https://people.csail.mit.edu/albert/bluez-intro/x232.html
# Taken from: https://people.csail.mit.edu/albert/bluez-intro/c212.html

from bluetooth import *
import time
import config
import serial  #library used to serialize data into string format (in order to send to Android)
from serial.serialutil import SerialException


# 주의 : https://raspberrypi.stackexchange.com/questions/60652/automatically-reconnect-paired-bluetooth-devices
# 에 나온것 처럼 Android 와 커넥션이 중단되면 , 리커넥할때 raspi 에서 기존에 Android 와 통신하는데 쓰였던 port를 닫고 다시 열어줘야한다.

def receiveMessages():
    server_sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
    port = 4  #raspi Bluetooth server 포트는 4로.
    server_sock.bind(("", port))  # 어떤 클라이언트와 연결하는지 모르므로 첫번쨰 인자는 ""
    server_sock.listen(1)

    client_sock, address = server_sock.accept()
    print("Accepted connection from " + str(address))

    data = client_sock.recv(1024)
    print("received [%s]" % data)

    client_sock.close()
    server_sock.close()

# 프로토타입 맥어드레스: 00: 01: 95: 18: 25: 90

def sendMessageTo(targetBluetoothMacAddress, info_to_send):
    port = 1
    sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
    sock.connect((targetBluetoothMacAddress, port))
    sock.send(info_to_send)
    sock.close()


def getTargetBluetoothDevice(target_device_name):
    #input : 디바이스 이름 예) "Supercrack_Android"
    #return 으로 종료 혹은 5초를 주기로 계속 반복하여 디바이스를 찾을때까지 계속 찾는다.
    nearby_devices = bluetooth.discover_devices(duration=10, lookup_names=True)
    target_address = None
    for bdaddr in nearby_devices:
        if target_device_name == bluetooth.lookup_name(bdaddr):
            target_address = bdaddr
            break
    if target_address is not None: #
        print("found target bluetooth device with address ", target_address)
        config.bluetooth_target_address = target_address
        #setup bluetooth server <TO RECEIVE DATA>
            #update time
        #setup bluetooth client <TO SEND DATA>
        #rfcomm_send(config.bluetooth_target_address, )
        return
    else:
        print("could not find target bluetooth device nearby")
        time.sleep(5)

def rfcomm_send(target_address, serialized_data, android_server_port_num):
    # target_address: 안드로이드 블루투스 서버의 맥주소.

    client_sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
    #port = bluetooth.get_available_port(bluetooth.RFCOMM)
    port =android_server_port_num  #
    try:
        client_sock.connect((target_address,port))
        client_sock.send(serialized_data)
    except BluetoothError as e:
        print(e)
    sock.close()

def serialize_video(video_file):
    return None