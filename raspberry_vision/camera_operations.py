import picamera
from picamera.array import PiRGBArray
from .NCS_MobileSSD import image_classify_with_timestamp
from threading import Thread, Event, Lock
import config

def record_and_capture(): # 이미 이 시점에서는 current time 과 안드로이드 타임이 동기화된 상황이기 때문에
    #  인자로 filename (타임스탬프)를 받지 않아도 된다.
    with picamera.PiCamera() as camera:
        camera.resolution = (640, 480)
        camera.start_preview()
        camera.start_recording(str(config.current_milli_time)+'.h264')

        camera.wait_recording(0.05)
        #only JPEG encoding is hardware accelerated (In case of .jpg, no video frame is dropped)
        capture_file_name = str(config.current_milli_time+50)+'.jpg'
        camera.capture(capture_file_name, use_video_port=True)
        #Spawn another thread within this thread to do CV on the .jpg
        tid_CV = Thread(target=image_classify_with_timestamp, args=(capture_file_name,))
        #Keep recording
        camera.wait_recording(0.05)
        camera.stop_recording()

def capture_for_cv(camera, capture_file_name):
    camera.capture(capture_file_name, use_video_port=True)

def circular_stream():
    # Video Streaming
    # 비디오 저장 (+ in-memory Streaming)
    with picamera.PiCamera() as camera:
        stream = picamera.PiCameraCircularIO(camera, seconds=15)
        camera.start_recording(stream, format='h264')
        try:
            while True:
                camera.wait_recording(1)
                if write_now():
                    # Keep recording for 15 seconds and only then write the
                    # stream to disk
                    camera.wait_recording(15)
                    write_video(stream, currentTime)
        finally:
            camera.stop_recording()