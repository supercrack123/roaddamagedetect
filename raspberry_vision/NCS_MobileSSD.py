## 출처: https://github.com/PINTO0309/MobileNet-SSD/blob/master/MultiStickSSD.py
# 위 코드를 용도에 맞게 변경하여 개발하고 있습니다.
import sys
import numpy as np
from os import system
import io, time
from os.path import isfile, join
from queue import Queue
from threading import Thread, Event, Lock
import re
from time import sleep
from mvnc import mvncapi as mvnc


devices = mvnc.enumerate_devices()
if len(devices) == 0:
    print("NCS / VPU 인식 실패")
    quit()

print(len(devices))




def keyboard(key, x, y):
    key = key.decode('utf-8')
    if key == 'q':
        lock.acquire()
        while len(frameBuffer) > 0:
            frameBuffer.pop()
        lock.release()
        for devnum in range(len(devices)):
            graphHandle[devnum].DeallocateGraph()
            devHandle[devnum].CloseDevice()
        print("\n\nFinished\n\n")
        sys.exit()


def camThread():
    t1 = time.perf_counter()
    global lastresults
    global fps

    s, img = cam.read()

    if not s:
        print("Could not get frame")
        return 0

    lock.acquire()
    if len(frameBuffer) > 10:
        del frameBuffer[0]

    frameBuffer.append(img)
    lock.release()
    res = None

    if not results.empty():
        res = results.get(False)
        lastresults = res
    else:
        print("results empty")

    ## Print FPS
    t2 = time.perf_counter()
    time1 = (t2 - t1)
    fps = " {:.1f} FPS".format(1 / time1)



def preprocess_image(src):
    image = cv2.resize(src, (224, 224))
    image = image - 95.2
    image = image * 0.005856

    return image

def image_classify_with_timestamp(img):
        im = preprocess_image(img)
        graph.queue_inference_with_fifo_elem(handle[0], handle[1], im.astype(np.float32), img)
        out, _ = handle[1].read_elem()
        results.put(out)

def image_classify(results, lock, frameBuffer, graph, handle):
    failure = 0
    sleep(1)
    while failure < 100:

        lock.acquire()
        if len(frameBuffer) == 0:
            lock.release()
            failure += 1
            continue

        img = frameBuffer[-1].copy()
        del frameBuffer[-1]
        failure = 0
        lock.release()

        im = preprocess_image(img)
        graph.queue_inference_with_fifo_elem(handle[0], handle[1], im.astype(np.float32), img)
        out, _ = handle[1].read_elem()
        results.put(out)




def overlay_on_image(display_image, object_info):
    global fps

    if isinstance(object_info, type(None)):
        return display_image

    num_valid_boxes = int(object_info[0])
    img_cp = display_image.copy()

    if num_valid_boxes > 0:

        for box_index in range(num_valid_boxes):
            base_index = 7 + box_index * 7
            if (not np.isfinite(object_info[base_index]) or
                    not np.isfinite(object_info[base_index + 1]) or
                    not np.isfinite(object_info[base_index + 2]) or
                    not np.isfinite(object_info[base_index + 3]) or
                    not np.isfinite(object_info[base_index + 4]) or
                    not np.isfinite(object_info[base_index + 5]) or
                    not np.isfinite(object_info[base_index + 6])):
                continue

            x1 = max(0, int(object_info[base_index + 3] * img_cp.shape[0]))
            y1 = max(0, int(object_info[base_index + 4] * img_cp.shape[1]))
            x2 = min(img_cp.shape[0], int(object_info[base_index + 5] * img_cp.shape[0]))
            y2 = min(img_cp.shape[1], int(object_info[base_index + 6] * img_cp.shape[1]))

            x1_ = str(x1)
            y1_ = str(y1)
            x2_ = str(x2)
            y2_ = str(y2)

            object_info_overlay = object_info[base_index:base_index + 7]

            min_score_percent = 75
            source_image_width = img_cp.shape[1]
            source_image_height = img_cp.shape[0]

            base_index = 0
            class_id = object_info_overlay[base_index + 1]
            percentage = int(object_info_overlay[base_index + 2] * 100)
            if (percentage <= min_score_percent):
                continue

            label_text = LABELS[int(class_id)] + " (" + str(percentage) + "%)"
            box_left = int(object_info_overlay[base_index + 3] * source_image_width)
            box_top = int(object_info_overlay[base_index + 4] * source_image_height)
            box_right = int(object_info_overlay[base_index + 5] * source_image_width)
            box_bottom = int(object_info_overlay[base_index + 6] * source_image_height)

            box_color = (255, 128, 0)
            box_thickness = 1
            cv2.rectangle(img_cp, (box_left, box_top), (box_right, box_bottom), box_color, box_thickness)

            label_background_color = (125, 175, 75)
            label_text_color = (255, 255, 255)

            label_size = cv2.getTextSize(label_text, cv2.FONT_HERSHEY_SIMPLEX, 0.5, 1)[0]
            label_left = box_left
            label_top = box_top - label_size[1]
            if (label_top < 1):
                label_top = 1
            label_right = label_left + label_size[0]
            label_bottom = label_top + label_size[1]
            cv2.rectangle(img_cp, (label_left - 1, label_top - 1), (label_right + 1, label_bottom + 1),
                          label_background_color, -1)
            cv2.putText(img_cp, label_text, (label_left, label_bottom), cv2.FONT_HERSHEY_SIMPLEX, 0.5, label_text_color,
                        1)

    cv2.putText(img_cp, fps, (235, 15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (38, 0, 255), 1, cv2.LINE_AA)
    return img_cp


print("press 'q' to quit!\n")

threads = []

for devnum in range(len(devices)):
    t = Thread(target=image_classify, args=(results, lock, frameBuffer, graph[devnum], graphHandle[devnum]))
    t.start()
    threads.append(t)
