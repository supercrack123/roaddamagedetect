import tensorflow as tf
import os
from object_detection.utils import dataset_util
from PIL import Image
import base64
import contextlib2
from object_detection.dataset_tools import tf_record_creation_util

num_shards = 10
output_filebase = 'train_dataset.record'

global xmin
global xmax
global ymax
global ymin
global filename1

flags = tf.app.flags
flags.DEFINE_string('output_path', 'tfrecord', 'Path to output TFRecord')
FLAGS = flags.FLAGS


def create_tf_example(xmin, xmax, ymin, ymax, filename1):
    # TODO(user): Populate the following variables from your example.
    height = 2760  # Image height
    width = 3680  # Image width
    cur_file_path = __file__
    cur_file_dir = os.path.dirname(cur_file_path)
    pos_file_dir = cur_file_dir + "/" + "Positive data"
    filename = cur_file_dir + "/" + filename1  # Filename of the image. Empty if image is not from file
    filename = str.encode(filename)
    image_format = b'jpeg'  # b'jpeg' or b'png'

    xmins = [xmin / width]  # List of normalized left x coordinates in bounding box (1 per box)
    xmaxs = [xmax / width]  # List of normalized right x coordinates in bounding box
    ymins = [ymin / height]  # List of normalized top y coordinates in bounding box (1 per box)
    ymaxs = [ymax / height]  # List of normalized bottom y coordinates in bounding box
    # (1 per box)

    img_file = open(filename1, 'rb')
    encoded_image_data = base64.b64encode(img_file.read())  # Encoded image bytes

    classdata = 'p'.encode()
    classes_text = [classdata]  # List of string class name of bounding box (1 per box)
    classes = [2]  # List of integer class id of bounding box (1 per box)

    #print('dsfajlsdjflasd')

    tf_example = tf.train.Example(features=tf.train.Features(feature={
        'image/height': dataset_util.int64_feature(height),
        'image/width': dataset_util.int64_feature(width),
        'image/filename': dataset_util.bytes_feature(filename),
        'image/source_id': dataset_util.bytes_feature(filename),
        'image/encoded': dataset_util.bytes_feature(encoded_image_data),
        'image/format': dataset_util.bytes_feature(image_format),
        'image/object/bbox/xmin': dataset_util.float_list_feature(xmins),
        'image/object/bbox/xmax': dataset_util.float_list_feature(xmaxs),
        'image/object/bbox/ymin': dataset_util.float_list_feature(ymins),
        'image/object/bbox/ymax': dataset_util.float_list_feature(ymaxs),
        'image/object/class/text': dataset_util.bytes_list_feature(classes_text),
        'image/object/class/label': dataset_util.int64_list_feature(classes),
    }))
    return tf_example


def main(_):
    writer = tf.python_io.TFRecordWriter(FLAGS.output_path)

    # TODO(user): Write code to read in your dataset to examples variable

    f = open('simpleTrainFullPhotosSortedFullAnnotations.txt')
    lines = f.readlines()

    for line in lines:
        linesplit = line.split()
        fntemp = linesplit[2].split('\\')
        filename1 = fntemp[1].split('.')
        filename1 = filename1[0] + '.jpg'
        print(filename1)

        i = 0

        for i in range(int(linesplit[3])):
            xmin = int(linesplit[4 * i + 4])
            print(xmin)
            xmax = xmin + int(linesplit[4 * i + 6])
            ymin = int(linesplit[4 * i + 5])
            ymax = ymin + int(linesplit[4 * i + 7])
            tf_example = create_tf_example(xmin, xmax, ymin, ymax, filename1)
            writer.write(tf_example.SerializeToString())

    writer.close()


if __name__ == '__main__':
    tf.app.run()