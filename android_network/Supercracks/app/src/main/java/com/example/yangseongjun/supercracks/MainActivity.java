package com.example.yangseongjun.supercracks;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.PowerManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.netcompss.ffmpeg4android.GeneralUtils;
import com.netcompss.ffmpeg4android.Prefs;
import com.netcompss.loader.LoadJNI;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * 위치 관리자를 이용해 내 위치를 확인하는 방법에 대해 알 수 있습니다.
 *
 * @author Mike
 *
 */
public class MainActivity extends AppCompatActivity {
    TextView textView;
    TextView textView2;

    Double latitude ;
    Double longitude ;
    String underground;

    Double Accel_x;
    Double Accel_y;
    Double Accel_z;
    Double Gyro_x;
    Double Gyro_y;
    Double Gyro_z;

    String weather_info;
    String Mac_add;

    byte[] file;
    String VideoPath = null;
    String vkLogPath = null;
    String VideoFolder = null;
    String workFolder = null;


    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

        VideoFolder = Environment.getExternalStorageDirectory().getAbsolutePath() + "/datas/";
        workFolder = getApplicationContext().getFilesDir() + "/";
        vkLogPath = workFolder + "vk.log";
        //GeneralUtils.copyFileToFolder(vkLogPath, VideoFolder);
        VideoPath = VideoFolder + "in.mp4";

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        GeneralUtils.checkForPermissionsMAndAbove(MainActivity.this, false);


        textView = (TextView) findViewById(R.id.textView);
        textView2 = findViewById(R.id.textView2);

        // 버튼 이벤트 처리
        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                GPS_On gps = new GPS_On();

                // 위치 정보 확인을 위해 정의한 메소드 호출
                gps.startLocationService();

            }
        });
    }

    private void Video_image() {

        if (GeneralUtils.checkIfFileExistAndNotEmpty(VideoPath)) {
            new Videoprocessing().execute();
        }
        else {
            Toast.makeText(getApplicationContext(), VideoPath + " not found", Toast.LENGTH_LONG).show();
        }
    }


    public class Videoprocessing extends AsyncTask<String, Integer, Integer>
    {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected Integer doInBackground(String... paths) {



            //String commandStr2 = "ffmpeg -y -i /sdcard/datas/in.mp4 -strict experimental -an -r 7 -ss 00:00:00.000 -t 00:00:03 /sdcard/datas/filename%09d.jpg";
            String commandStr = "ffmpeg -y -i /sdcard/datas/in.mp4 -strict experimental -an -r 7 /sdcard/datas/filename%09d.jpg";

            LoadJNI vk = new LoadJNI();
            try {
                vk.run(GeneralUtils.utilConvertToComplex(commandStr), workFolder, getApplicationContext());
                //GeneralUtils.copyFileToFolder(vkLogPath, VideoFolder);
            }
            catch (Throwable e) {
                Log.e(Prefs.TAG, "vk run exeption.", e);
            }

            return Integer.valueOf(0);
        }

    }




    class GPS_On{

        private void startLocationService() {
            LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

            GPSListener gpsListener = new GPSListener();
            long minTime = 1000;
            float minDistance = 0;

            try {
                manager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER,
                        minTime,
                        minDistance,
                        gpsListener);

                manager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER,
                        minTime,
                        minDistance,
                        gpsListener);

                Location lastLocation = manager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if (lastLocation != null) {
                    latitude = lastLocation.getLatitude();
                    longitude = lastLocation.getLongitude();

                    textView.setText("내 위치 : " + latitude + ", " + longitude);
                    Toast.makeText(getApplicationContext(), "Last Known Location : " + "Latitude : " + latitude + "\nLongitude:" + longitude, Toast.LENGTH_LONG).show();
                }
            } catch(SecurityException ex) {
                ex.printStackTrace();
            }

            Toast.makeText(getApplicationContext(), "위치 확인이 시작되었습니다. 로그를 확인하세요.", Toast.LENGTH_SHORT).show();

        }

        private class GPSListener implements LocationListener {

            public void onLocationChanged(Location location) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();

                String msg = "Latitude : "+ latitude + "\nLongitude:"+ longitude;
                Log.i("GPSListener", msg);

                textView.setText("내 위치 : " + latitude + ", " + longitude);
                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();

                getWeatherData(latitude, longitude);

                get_ground_data(latitude, longitude);

                Video_image();

                // 이미지 데이터 변화에 따라, 차후 라즈베리파이로부터 받을 gps 값을 포함 여러 값을 바인딩해서 전송해야 할 것!!
                Networking network = new Networking("http://private-anon-4a296096d7-supercrack.apiary-mock.com/defect");
                network.execute();

            }

            public void onProviderDisabled(String provider) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

        }

    }


    private void getWeatherData( double lat, double lon){

        String url = "http://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + lon + "&units=metric&appid=7fb41e54e95bbcbcd8ce6332162b633b";
        ReceiveWeatherTask receiveUseTask = new ReceiveWeatherTask();
        receiveUseTask.execute(url);

    }

    private class ReceiveWeatherTask extends AsyncTask<String, Void, JSONObject>{

        protected void onPreExecute(){
            super.onPreExecute();
        }

        protected JSONObject doInBackground(String... datas){

            try{
                HttpURLConnection conn = (HttpURLConnection) new URL(datas[0]).openConnection();
                conn.setConnectTimeout(10000);
                conn.setReadTimeout(10000);
                conn.connect();

                if(conn.getResponseCode() == HttpURLConnection.HTTP_OK){
                    InputStream is = conn.getInputStream();
                    InputStreamReader reader = new InputStreamReader(is);
                    BufferedReader in = new BufferedReader(reader);

                    String readed;
                    while((readed = in.readLine()) != null){
                        JSONObject jObject = new JSONObject(readed);
                        return jObject;
                    }
                } else{
                    return null;
                }
                return null;
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(JSONObject result){
            if(result != null){
                String nowTemp = "";
                String description = "";

                try{

                    weather_info = result.getJSONArray("weather").getJSONObject(0).getString("main");
                    //description = result.getJSONArray("weather").getJSONObject(0).getString("description");
                    //nowTemp = result.getJSONObject("main").getString("temp");
                }
                catch(JSONException e){
                    e.printStackTrace();
                }

                textView.setText("weather is " + weather_info);


            }
        }

    }


    private void get_ground_data(double lat, double lon){

        String url2 = "https://dapi.kakao.com/v2/local/geo/coord2address.json?"+"x=" + lon + "&y=" + lat + "&input_coord=WGS84";
        Kakaoapi receive_GD_data = new Kakaoapi();
        receive_GD_data.execute(url2);

    }


    class Kakaoapi extends AsyncTask<String, Void, JSONObject> {

        protected void onPreExecute(){
            super.onPreExecute();
        }


        protected JSONObject doInBackground(String... datas){
            try{
                HttpURLConnection conn = (HttpURLConnection) new URL(datas[0]).openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Authorization", "KakaoAK " + "25d009a3aac07153e6c6a688804d6e4e");
                conn.setConnectTimeout(1000);
                conn.setReadTimeout(1000);
                conn.connect();

                if(conn.getResponseCode() == HttpURLConnection.HTTP_OK){
                    InputStream is = conn.getInputStream();
                    InputStreamReader reader = new InputStreamReader(is);
                    BufferedReader in = new BufferedReader(reader);

                    String readed;
                    while((readed = in.readLine()) != null){
                        JSONObject jObject = new JSONObject(readed);
                        return jObject;
                    }
                } else{
                    return null;
                }
                return null;
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;




        }

        protected void onPostExecute(JSONObject result){
            if(result != null){

                try{
                    JSONArray document = result.getJSONArray("documents");
                    JSONObject road_address = document.getJSONObject(0).getJSONObject("road_address");
                    underground = road_address.getString("underground_yn");
                }
                catch(JSONException e){
                    e.printStackTrace();
                }
                textView2.setText("result : " + underground);
            }
        }



    }

    public class Networking extends AsyncTask<String, Void, Boolean> {

        String urlString ;

        private final String TAG = "post json example";

        public Networking(String urlString) {
            this.urlString = urlString;
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Boolean doInBackground(String... data) {
            boolean status = false;

            String response = ""; String response2 = "";
            Log.e(TAG, "2 - pre Request to response...");

            try {
                response = performPostCall(urlString);
                response2 = performPutCall(urlString + "/" +response);
                Log.e(TAG, "response is " + response);
                Log.e(TAG, "response2 is " + response2.toString());
                status = true;
            } catch (Exception e) {
                Log.e(TAG, "Error ...");
                status = false;
            }

            return status;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            Log.e(TAG, "7 - onPostExecute ...");

            if (result) {
                Log.e(TAG, "8 - Update UI ...");

            } else {
                Log.e(TAG, "8 - Finish ...");

            }

        }

        public String performPostCall(String requestURL) {

            URL url;
            String response = "";
            Boolean isShade ;

            if (underground == "y" | underground == "Y")
                isShade = true;
            else
                isShade = false;


            try {
                url = new URL(requestURL);

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(20000);
                conn.setConnectTimeout(20000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                conn.setRequestProperty("content-type", "application/json");

                Log.e(TAG, "11 - url : " + requestURL);


                JSONObject root = new JSONObject();
                JSONArray Gyro = new JSONArray();
                JSONArray Acceleration = new JSONArray();


                double[] gyro = new double[3];
                gyro[0] = -1.2; gyro[1] = 0.43; gyro[2] = 2.43;
                Gyro.put(gyro[0]); Gyro.put(gyro[1]); Gyro.put(gyro[2]);

                double[] acceleration = new double[3];
                acceleration[0] = -0.3; acceleration[1] = 4.3; acceleration[2] = 2.2; //sample value
                Acceleration.put(acceleration[0]); Acceleration.put(acceleration[1]); Acceleration.put(acceleration[2]); //sample value

                root.accumulate("latitude",latitude);
                root.accumulate("longitude",longitude);
                root.accumulate("isShade",isShade);
                root.accumulate("weather",weather_info);
                root.accumulate("userId","killerpug");
                root.accumulate("deviceId","C1-76-76-77-D9-C1");
                root.accumulate("gyro", Gyro);
                root.accumulate("acceleration", Acceleration);

                DataOutputStream os = new DataOutputStream(conn.getOutputStream());
                os.writeBytes(root.toString());

                int responseCode = conn.getResponseCode();

                Log.e(TAG, "13 - responseCode : " + responseCode);

                if (responseCode == 200) {
                    Log.e(TAG, "14 - HTTP_OK");

                    // String real_message = conn.getHeaderField("Location");
                    // response = real_message;
                    String line;
                    BufferedReader br = new BufferedReader(new InputStreamReader(
                            conn.getInputStream()));
                    while ((line = br.readLine()) != null) {
                        response += line;
                    }

                } else {
                    Log.e(TAG, "14 - False - HTTP_OK");
                    response = "";
                }
                conn.disconnect();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return response;
        }



        public byte[] bitmapToByteArray( Bitmap bitmap ) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream() ;
            bitmap.compress( Bitmap.CompressFormat.JPEG, 100, stream) ;
            byte[] byteArray = stream.toByteArray() ;
            return byteArray ;
        }


        public byte[] get_image(Bitmap myBitmap){
            ByteArrayOutputStream stream = new ByteArrayOutputStream() ;
            myBitmap.compress( Bitmap.CompressFormat.JPEG, 75, stream) ;
            byte[] byteArray = stream.toByteArray() ;
            return byteArray;
        }

        public String performPutCall(String requestURL) {

            String FolderPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/datas/";
            String ImagePath = FolderPath + "Notebook.jpg"; // 현재 datas 폴더에 파일 지웠음ㅠ 다시 만들어줘야 함.
            File imgFile = new File(ImagePath);
            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

            byte[] image_file = new byte[1000 * 1000];
            URL url;
            String response = "";

            image_file = get_image(myBitmap);

            try {
                url = new URL(requestURL);

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(20000);
                conn.setConnectTimeout(20000);
                conn.setRequestMethod("PUT");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                conn.setRequestProperty("content-type", "multipart/form-data");
                JSONObject root = new JSONObject();
                root.accumulate("file",image_file);
                DataOutputStream os = new DataOutputStream(conn.getOutputStream());

                os.writeBytes(root.toString());
                int responseCode = conn.getResponseCode();

                Log.e(TAG, "responseCode : " + responseCode);

                if (responseCode == 201) {
                    response = "image is uploaded";
                } else {
                    response = "image is not uploaded";
                }
                conn.disconnect();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return response;
        }


    }

}