package com.example.yangseongjun.network;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class MainActivity extends AppCompatActivity {

    Button network_button;
    TextView get_result;
    ImageView get_image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        network_button = findViewById(R.id.Button1);
        get_result = findViewById(R.id.text1);
        get_image = findViewById(R.id.imageView);


        network_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Networking connecting = new Networking("http://private-anon-4a296096d7-supercrack.apiary-mock.com/defect");
                connecting.execute();
            }
        });

    }

    public class Networking extends AsyncTask<String, Void, Boolean> {

        String urlString ;

        private final String TAG = "post json example";

        public Networking(String urlString) {
         this.urlString = urlString;
       }

        @Override
        protected void onPreExecute() {
            Log.e(TAG, "1 - RequestVoteTask is about to start...");
        }

        @Override
        protected Boolean doInBackground(String... data) {
            boolean status = false;

            String response = ""; String response2 = "";
            Log.e(TAG, "2 - pre Request to response...");

            try {
                response = performPostCall(urlString);
                response2 = performPutCall(urlString + "/" +response);
              //  response2 = performPutCall(urlString + "/4l3h54l3-5345gsd-t34t34c-c4t34f");
                Log.e(TAG, "response is " + response);
                Log.e(TAG, "response2 is " + response2.toString());
                status = true;
            } catch (Exception e) {
                Log.e(TAG, "Error ...");
                status = false;
            }

            return status;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            //
            Log.e(TAG, "7 - onPostExecute ...");

            if (result) {
                Log.e(TAG, "8 - Update UI ...");

            } else {
                Log.e(TAG, "8 - Finish ...");

                // displayLoding(false);
                // finish();
            }

        }

        public String performPostCall(String requestURL) {


            URL url;
            String response = "";
            
            try {
                url = new URL(requestURL);

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(20000);
                conn.setConnectTimeout(20000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                conn.setRequestProperty("content-type", "application/json");

                Log.e(TAG, "11 - url : " + requestURL);


                JSONObject root = new JSONObject();
                JSONArray Gyro = new JSONArray();
                JSONArray Acceleration = new JSONArray();


                double[] gyro = new double[3];
                gyro[0] = -1.2; gyro[1] = 0.43; gyro[2] = 2.43;
                Gyro.put(gyro[0]); Gyro.put(gyro[1]); Gyro.put(gyro[2]);

                double[] acceleration = new double[3];
                acceleration[0] = -0.3; acceleration[1] = 4.3; acceleration[2] = 2.2;
                Acceleration.put(acceleration[0]); Acceleration.put(acceleration[1]); Acceleration.put(acceleration[2]);

                root.accumulate("latitude",126.9363341);
                root.accumulate("longitude",37.5618584);
                root.accumulate("isShade",false);
                root.accumulate("weather","rainy");
                root.accumulate("userId","killerpug");
                root.accumulate("deviceId","C1-76-76-77-D9-C1");
                root.accumulate("gyro", Gyro);
                root.accumulate("acceleration", Acceleration);

                DataOutputStream os = new DataOutputStream(conn.getOutputStream());
                os.writeBytes(root.toString());

                int responseCode = conn.getResponseCode();

                Log.e(TAG, "13 - responseCode : " + responseCode);

                if (responseCode == 200) {
                    Log.e(TAG, "14 - HTTP_OK");

                   // String real_message = conn.getHeaderField("Location");
                   // response = real_message;
                    String line;
                    BufferedReader br = new BufferedReader(new InputStreamReader(
                            conn.getInputStream()));
                    while ((line = br.readLine()) != null) {
                        response += line;
                    }

                } else {
                    Log.e(TAG, "14 - False - HTTP_OK");
                    response = "";
                }
                conn.disconnect();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return response;
        }


        /*
        public byte[] bitmapToByteArray( Bitmap bitmap ) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream() ;
            bitmap.compress( Bitmap.CompressFormat.JPEG, 100, stream) ;
            byte[] byteArray = stream.toByteArray() ;
            return byteArray ;
        }
        */

        public byte[] get_image(Bitmap myBitmap){
                ByteArrayOutputStream stream = new ByteArrayOutputStream() ;
                myBitmap.compress( Bitmap.CompressFormat.JPEG, 75, stream) ;
                byte[] byteArray = stream.toByteArray() ;
                return byteArray;
        }

        public String performPutCall(String requestURL) {

            String FolderPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/datas/";
            String ImagePath = FolderPath + "Notebook.jpg";
            File imgFile = new File(ImagePath);
            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

            byte[] image_file = new byte[1000 * 1000];
            URL url;
            String response = "";

            image_file = get_image(myBitmap);

            try {
                url = new URL(requestURL);

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(20000);
                conn.setConnectTimeout(20000);
                conn.setRequestMethod("PUT");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                conn.setRequestProperty("content-type", "multipart/form-data");

                Log.e(TAG, "11 - url : " + requestURL);



                JSONObject root = new JSONObject();

                root.accumulate("file",image_file);

                Log.e(TAG, "it is working!!");

                DataOutputStream os = new DataOutputStream(conn.getOutputStream());

                os.writeBytes(root.toString());
                int responseCode = conn.getResponseCode();

                Log.e(TAG, "13 - responseCode : " + responseCode);

                if (responseCode == 201) {
                    Log.e(TAG, "14 - HTTP_OK");
                    response = "image is uploaded";
                } else {
                    Log.e(TAG, "14 - False - HTTP_OK");
                    response = "image is not uploaded";
                }
                conn.disconnect();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return response;
        }


    }

}
