#!/usr/bin/env python
# coding: utf-8

# In[40]:


import numpy as np
import sys
import tarfile
import tensorflow as tf # tensorflow v1.4.1 일 때 작성된 코드
import zipfile
import os
from collections import defaultdict
from io import StringIO
from matplotlib import pyplot as plt
from PIL import Image

# 해당 코드는 Road Damage Detection Using Deep Neural Networks with Images Captured Through a Smartphone, Hiroya Maeda (2018) reference code를 참고함.

from utils import label_map_util # tensorflow object detection api 코드 
from utils import visualization_utils as vis_util # tensorflow object api 코드

PATH_TO_CKPT = os.path.join(os.getcwd(), "model", "RoadDamage.pb") # 모델 load

PATH_TO_LABELS = os.path.join(os.getcwd(), 'model', 'label_map.pbtxt') # label text
NUM_CLASSES = 7

general_road_path = os.path.join(os.getcwd(), "road_data")
road_damage_data_path = os.path.join(os.getcwd(), "road_damage_data")

# gray data
gray_general_road_path = os.path.join(os.getcwd(), "road_gray")
gray_road_damage_data_path = os.path.join(os.getcwd(), "gray_road_damage_data")


IMAGE_SIZE = (12, 8) # 디텍션된 이미지를 보여줄 때 사이즈 조정

def load_image_into_numpy_array(image): # 모델 인풋에 들어갈 numpy array size를 조정
    (im_width, im_height) = image.size
    return np.array(image.getdata()).reshape((im_height, im_width, 3)).astype(np.uint8)

def show_detection(data_path):
    TEST_IMAGE_PATHS=[os.path.join(data_path, f) for f in os.listdir(data_path)]

    with detection_graph.as_default():
        with tf.Session(graph=detection_graph) as sess:
            # Definite input and output Tensors for detection_graph
            image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
            # Each box represents a part of the image where a particular object was detected.
            detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
            # Each score represent how level of confidence for each of the objects.
            # Score is shown on the result image, together with the class label.
            detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
            detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')
            num_detections = detection_graph.get_tensor_by_name('num_detections:0')
        
            for image_path in TEST_IMAGE_PATHS:
                image = Image.open(image_path).convert('RGB') # 테스트 데이터를 icrawler로 임의로 크롤링하였기에 rgba형태가 있을 수 있어 rgb로 치환
            
                image_np = load_image_into_numpy_array(image)
                # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
                image_np_expanded = np.expand_dims(image_np, axis=0)
            
                # Actual detection.
                (boxes, scores, classes, num) = sess.run(
                    [detection_boxes, detection_scores, detection_classes, num_detections],
                    feed_dict={image_tensor: image_np_expanded})
            
                # Visualization of the results of a detection.
                vis_util.visualize_boxes_and_labels_on_image_array(
                    image_np,
                    np.squeeze(boxes),
                    np.squeeze(classes).astype(np.int32),
                    np.squeeze(scores),
                    category_index,
                    min_score_thresh=0.5, # threshold
                    use_normalized_coordinates=True,
                    line_thickness=8)
                plt.figure(figsize=IMAGE_SIZE)
                plt.imshow(image_np)


# In[41]:


detection_graph = tf.Graph()
with detection_graph.as_default():
    od_graph_def = tf.GraphDef()
    with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
        serialized_graph = fid.read()
    od_graph_def.ParseFromString(serialized_graph)
    tf.import_graph_def(od_graph_def, name='')


# In[42]:


label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
category_index = label_map_util.create_category_index(categories)


# In[43]:


show_detection(general_road_path)


# In[44]:


show_detection(gray_general_road_path)


# In[45]:


show_detection(road_damage_data_path)


# In[46]:


show_detection(gray_road_damage_data_path)


# In[ ]:




