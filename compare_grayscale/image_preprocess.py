#!/usr/bin/env python
# coding: utf-8

# In[16]:


import os
from os.path import isfile, join
import urllib.request
from PIL import Image
import io
import shutil
import csv
import filetype
import numpy as np
import cv2

general_road_path = os.path.join(os.getcwd(), "road_data")
crack_path = os.path.join(os.getcwd(), "crack_data")
pothole_path = os.path.join(os.getcwd(), "pothole_data")
data_root = os.getcwd()

gray_general_road_path = os.path.join(os.getcwd(), "road_gray")
gray_crack_path = os.path.join(os.getcwd(), "crack_gray")
gray_pothole_path = os.path.join(os.getcwd(), "pothole_gray")

def file_rename(name, data_root):
    datafile = [f for f in os.listdir(data_root)]
    
    i = 1
    for filename in datafile:
        kind = filetype.guess( os.path.join(data_root, filename) )
        #os.rename(os.path.join(data_root, filename), os.path.join(data_root, name + "_" + filename)) # 파일 확장자와 결합
        os.rename(os.path.join(data_root, filename), os.path.join(data_root, filename) + "." + kind.extension) # 파일 확장자와 결합
        i+=1

def make_grayscale(dirName, data_path):
    if not os.path.exists(dirName):
        os.mkdir(dirName)
    datafile = [f for f in os.listdir(data_path)]
    for file in datafile:
        img = Image.open(os.path.join(data_path, file)).convert('L').save(os.path.join(dirName,"gray_" + file))


# In[ ]:




